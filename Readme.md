# Topological Map
## A project exploring the use of a topological map user interface for home automation/control/monitoring projects

![Banner showing topological map showing a few rooms](https://bytebucket.org/harrisonhjones/topological-map/raw/master/Banner.png "Banner showing topological map showing a few rooms")

## Motivation
I've been interested for a while in the topic of HCI (Human Computer Interaction) and home automation/monitoring. This project was the result of me asking myself: "apart from a floor layout or blueprint how else could the spaces of a home be visualized in a way that was intuitive and simple?" My answer was a topological map of a home's room and doorways. 

## Object Framework
Before even starting on the actual work of visualizing the map I needed to determine the struture of each object. Ie, what is a room or a door and what qualities does it have? 

### Rooms
Every room object is constructed with the following parameters: id, label, floor, fillStyle, and "extras". The id uniquely identifies the room and is used when joining two rooms by a door or doorway. The label is used when visualizing the room and simply contains a short text description of the room. The floor is a positive integer and identifies which floor the room is on. This number is used when visualizing the map; rooms on the same floor are grouped togethor. Floor "0" is a special floor and represents the "outside" of the house. FillStyle is simple what color the room should appear on the map. Later this parameter may be removed in favor of "smart" fillyStyles which change to reflect changing room states (red for emergency or alarm, green to "ok", yellow for motion or movement, etc). Finally "extras" is a special parameter that is not used currently. In the future it may be used to pass objects such as devices or sensors to the room to be represented and manipulated on the map.

### Doors
Every door object is constructed with the following parameters: id, type, room1ID, room2ID, and "extras". The id uniquely identifies the door and is used when visualizing the map. The type is a string which describes the type of door. Types are arbitary and can be anything. Examples of door types: Open, Garage Door, SC (standard clock wise), SCC (standard counter clock wise), sliding, etc. The "Open" type is special. If a door is marked as "Open" it will not be drawn on the map; only a simply line will be drawn between the connected rooms. The reasoning behind this visualization choice is that a "Open" door is simply a doorway and not really a door in the traditional sense. To show open doorways in the same way as standard doors would be confusing. RoomID1 and RoomID2 are simply integers containing room ids for the two rooms this door connects. Finally "extras" is a special parameter that is not used currently. In the future it may be used to pass objects such as devices or sensors to the door to be represented and manipulated on the map.

## Visualization of the Map
The map is drawn onto a HTML Canvas object. Rooms are rectanges with customizable background color and label text. Links between rooms are visualized with straight black lines. Doors (not empty doorways) are placed on link lines and are small pink rectangle with the door's ID as the label text. The first incarnation of the map placed all the rooms evenly spaced around the edge of a large circle as seen below.

![Image of the first version of the map](https://bytebucket.org/harrisonhjones/topological-map/raw/master/OriginalCircleDesign.png "Image of the first version of the map")

This version of the map was extremly hard to read. You could not determine which floor each room was on and rooms which were physically located near eachother were not on the map. Attempting to place rooms by location led to a map which had several overlapping doors icons and was even harder to read. This lead me seperate rooms by floors and to visualize each floor as a seperate circular entity. The result of this seperation can be seen below.

![Image of the current version of the map](https://bytebucket.org/harrisonhjones/topological-map/raw/master/Screenshot.png "Image of the current version of the map")

The major problem with this method of visualization is that the maps tend to be very wide. To counter this I looked into being able to "pan" the canvas. Several stack exchange posts later the map could be panned. An demo page showing off the current state of the map can be seen [here](http://jsfiddle.net/harrisonhjones/rbLedbc6/ "Topological Map Demo") or by opening the supplied "demo.html" in your browser.

## Going Forward
Below is a list of things that need to be figured out

* While each room and each door as infact "clickable" I haven't decided how to handle the clicks
* Room and door color should react and by dynamic, reacting to which "layer" (security, comfort, etc) is current selected and what, if any, events have recently occured. 
* How should sensors like cameras, temperature sensors, motion sensors, etc be represented, encapsulated inside the room/door object, and interacted with?
* How should actuators like garage door motors, drivable window blinks, door locks, etc be represented, encapsulated inside the room/door object, and interacted with?